use mcts::transposition_table::*;
use mcts::tree_policy::*;
use mcts::*;
use rand::distributions::WeightedIndex;
use rand::prelude::Distribution;
use std::ops::Neg;
use std::sync::Arc;
use std::sync::Mutex;

use crate::eval::evaluate;
use crate::eval::EvalInput;
use crate::eval::EvalOutput;
use crate::model::Model;

pub fn sample_move(
    tree: &mcts::SearchTree<ChessMcts>,
    rng: &mut impl rand::Rng,
) -> chess::ChessMove {
    let root = tree.root_node();
    let moves: Vec<_> = root.moves().map(|info| info.get_move()).collect();
    let weights: Vec<_> = root.moves().map(|info| info.visits()).collect();

    let idx = WeightedIndex::new(weights).unwrap().sample(rng);
    *moves[idx]
}

#[derive(Clone)]
pub struct ChessGame(pub chess::Game);

impl ChessGame {
    fn new() -> Self {
        Self(chess::Game::new())
    }
}

impl Default for ChessGame {
    fn default() -> Self {
        Self::new()
    }
}

impl GameState for ChessGame {
    type Move = chess::ChessMove;

    type Player = chess::Color;

    type MoveList = Vec<Self::Move>;

    fn current_player(&self) -> Self::Player {
        self.0.side_to_move()
    }

    fn available_moves(&self) -> Self::MoveList {
        if self.0.can_declare_draw() {
            return vec![];
        }
        let moveiter = chess::MoveGen::new_legal(&self.0.current_position());
        let moves = moveiter.collect();
        moves
    }

    fn make_move(&mut self, mov: &Self::Move) {
        let valid = self.0.make_move(*mov);
        assert!(valid);
    }
}

pub struct ChessEvaluator {
    model: Arc<Mutex<Model>>,
}

impl ChessEvaluator {
    pub fn new(model: Model) -> Self {
        Self {
            model: Arc::new(Mutex::new(model)),
        }
    }

    pub fn from_arc(model: Arc<Mutex<Model>>) -> Self {
        Self { model }
    }
}

#[derive(Debug, Copy, Clone)]
pub struct Selfish<T> {
    pub value: T,
    pub player: chess::Color,
}

impl Selfish<f64> {
    pub fn to_centipawns(&self) -> i32 {
        (self.value * 100.0 * 10.0) as i32
    }
}

#[derive(Debug, Copy, Clone)]
pub struct Objective<T>(pub T);

impl From<chess::GameResult> for Objective<f64> {
    fn from(value: chess::GameResult) -> Self {
        let eval = match value {
            chess::GameResult::WhiteCheckmates => 1.,
            chess::GameResult::WhiteResigns => -1.,
            chess::GameResult::BlackCheckmates => -1.,
            chess::GameResult::BlackResigns => 1.,
            chess::GameResult::Stalemate => 0.,
            chess::GameResult::DrawAccepted => 0.,
            chess::GameResult::DrawDeclared => 0.,
        };
        Objective(eval)
    }
}

impl<T: Neg + Neg<Output = T>> Selfish<T> {
    pub fn to_objective(self) -> Objective<T> {
        match self.player {
            chess::Color::White => Objective(self.value),
            chess::Color::Black => Objective(-self.value),
        }
    }
}

impl<T: Neg + Neg<Output = T>> Objective<T> {
    pub fn to_selfish(self, view: chess::Color) -> Selfish<T> {
        match view {
            chess::Color::White => Selfish {
                value: self.0,
                player: view,
            },
            chess::Color::Black => Selfish {
                value: -self.0,
                player: view,
            },
        }
    }
}

impl Evaluator<ChessMcts> for ChessEvaluator {
    type StateEvaluation = Selfish<f64>;

    fn evaluate_new_state(
        &self,
        state: &<ChessMcts as MCTS>::State,
        moves: &MoveList<ChessMcts>,
        _handle: Option<SearchHandle<ChessMcts>>,
    ) -> (Vec<MoveEvaluation<ChessMcts>>, Self::StateEvaluation) {
        let player = state.current_player();
        if let Some(res) = state.0.result() {
            return (
                vec![0.; moves.len()],
                Objective::from(res).to_selfish(player),
            );
        }

        if state.0.can_declare_draw() {
            return (
                vec![0.; moves.len()],
                Selfish {
                    value: 0.0,
                    player: state.current_player(),
                },
            );
        }

        let board = state.0.current_position();
        if board.combined().popcnt() == 2 {
            return (
                vec![1.0 / moves.len() as f64; moves.len()],
                Selfish {
                    value: 0.0,
                    player: state.current_player(),
                },
            );
        }
        let EvalOutput { evaluation, policy } = evaluate(
            &self.model.try_lock().unwrap(),
            EvalInput {
                board,
                view: player,
            },
        );
        let move_eval = {
            moves
                .iter()
                .map(|mov| policy.iter().find(|v| v.mov == *mov))
                .map(|m| m.map(|m| m.value).unwrap_or(0.))
                .collect()
        };
        (move_eval, evaluation)
    }

    fn evaluate_existing_state(
        &self,
        _state: &<ChessMcts as MCTS>::State,
        existing_evaln: &Self::StateEvaluation,
        _handle: SearchHandle<ChessMcts>,
    ) -> Self::StateEvaluation {
        *existing_evaln
    }

    fn interpret_evaluation_for_player(
        &self,
        evaluation: &Self::StateEvaluation,
        player: &Player<ChessMcts>,
    ) -> i64 {
        let Selfish { value: eval, .. } = evaluation.to_objective().to_selfish(*player);
        return (eval * 100.) as i64;
    }
}

pub struct ChessMcts {}

impl TranspositionHash for ChessGame {
    fn hash(&self) -> u64 {
        self.0.current_position().get_hash()
    }
}

impl MCTS for ChessMcts {
    type State = ChessGame;
    type Eval = ChessEvaluator;
    type NodeData = ();
    type ExtraThreadData = ();
    type TreePolicy = AlphaGoPolicy;
    type TranspositionTable = ApproxTable<Self>;

    fn cycle_behaviour(&self) -> CycleBehaviour<Self> {
        CycleBehaviour::Ignore
    }
}

/// returns the reward (1 to -1) of the move from the point of view from the current player
pub fn move_reward_norm(move_info: &MoveInfo<ChessMcts>) -> f64 {
    move_info.sum_rewards() as f64 / move_info.visits() as f64 / 100.0
}
