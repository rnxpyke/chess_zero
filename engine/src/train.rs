use chess::GameResult;
use mcts::GameState;
use tch::Tensor;

use crate::{
    chess_board_to_tensor,
    eval::moves_to_policy_label,
    model::TrainSample,
    tree_search::{move_reward_norm, ChessMcts, Objective, Selfish},
};

#[derive(Debug)]
struct Sample<F> {
    board: chess::Board,
    moves: Vec<(chess::ChessMove, u64)>,
    boost_eval: Selfish<f64>,
    result: F,
}

impl<F> Sample<F> {
    fn with_result<R>(self, result: R) -> Sample<R> {
        Sample {
            board: self.board,
            moves: self.moves,
            boost_eval: self.boost_eval,
            result,
        }
    }
}

fn create_sample(mcts: &mcts::MCTSManager<ChessMcts>) -> Sample<()> {
    let tree = mcts.tree();
    let board = tree.root_state();
    let root = tree.root_node();
    let mut moves = vec![];

    for mov_info in root.moves() {
        let mov = *mov_info.get_move();
        let visits = mov_info.visits();
        moves.push((mov, visits));
    }

    let evaluation = {
        let var = mcts.principal_variation_info(1)[0];
        let eval = Selfish {
            player: board.current_player(),
            value: move_reward_norm(&var),
        };
        eval
    };

    Sample {
        board: board.0.current_position(),
        moves,
        boost_eval: evaluation,
        result: (),
    }
}

fn create_train_tensor(
    board: chess::Board,
    evaluation: Selfish<f64>,
    moves: &[(chess::ChessMove, u64)],
) -> TrainSample {
    let player = board.side_to_move();
    let input = chess_board_to_tensor(&board, player).view([1, 2, 6, 8, 8]);
    let value = Tensor::from(evaluation.value as f32).view([1]);
    let policy = moves_to_policy_label(moves, player).view([1, 64 * 64]);
    TrainSample {
        input,
        value,
        policy,
    }
}

fn boost_sample_to_training_tensors<A>(sample: &Sample<A>) -> TrainSample {
    create_train_tensor(sample.board, sample.boost_eval, &sample.moves)
}

fn sample_to_training_tensors(sample: &Sample<GameResult>) -> TrainSample {
    let player = sample.board.side_to_move();
    let evaluation = Objective::from(sample.result).to_selfish(player);
    create_train_tensor(sample.board, evaluation, &sample.moves)
}

/*
fn generate_self_play_sample(
    args: &ChessMlArgs,
    model: Arc<Mutex<Model>>,
    rng: &mut impl rand::Rng,
    boards_sender: &std::sync::mpsc::SyncSender<chess::Board>,
    evals_sender: &std::sync::mpsc::SyncSender<f64>,
) -> eyre::Result<TrainSample> {
    use chessml::tree_search::*;

    let mut game = chess::Game::new();
    let mut samples: Vec<Sample<()>> = vec![];
    while game.result().is_none() {
        if game.can_declare_draw() {
            game.declare_draw();
            assert!(game.result().is_some());
            break;
        }

        let mut mcts = create_mcts(model.clone(), &game, args.exploration);
        mcts.playout_n(args.rollouts);
        if args.debug_moves {
            go_info(&mcts);
        }
        let evaluation = {
            let var = mcts.principal_variation_info(1)[0];
            let Objective(eval) = Selfish {
                player: game.current_position().side_to_move(),
                value: move_reward_norm(var),
            }
            .to_objective();
            eval
        };
        dbg!(evaluation);
        samples.push(create_sample(&mcts));
        let mov = sample_move(mcts.tree(), rng);
        game.make_move(mov);
        boards_sender.send(game.current_position()).unwrap();
        evals_sender.send(evaluation).unwrap();
    }

    let result = game.result().unwrap();
    let samples: Vec<_> = samples.into_iter().map(|s| s.with_result(result)).collect();
    let train_samples: Vec<_> = samples
        .iter()
        .map(sample_to_training_tensors)
        .chain(samples.iter().map(boost_sample_to_training_tensors))
        .collect();
    let train_sample = stack_samples(train_samples);

    Ok(train_sample)
}
*/

/*
fn save_sample(args: &ChessMlArgs, sample: &TrainSample) -> eyre::Result<()> {
    let sample_time = chrono::offset::Utc::now().timestamp_millis();
    let sample_folder = args.sample_dir.join(format!("{}-sample", sample_time));
    std::fs::create_dir_all(&sample_folder)?;
    sample.input.save(sample_folder.join("input"))?;
    sample.value.save(sample_folder.join("value"))?;
    sample.policy.save(sample_folder.join("policy"))?;
    Ok(())
}

fn load_samples(args: &ChessMlArgs) -> eyre::Result<Vec<TrainSample>> {
    let mut samples = vec![];
    for entry in std::fs::read_dir(&args.sample_dir)? {
        let sample_path = entry?.path();
        let input = tch::Tensor::load(sample_path.join("input"))?;
        let value = tch::Tensor::load(sample_path.join("value"))?;
        let policy = tch::Tensor::load(sample_path.join("policy"))?;
        samples.push(TrainSample {
            input,
            value,
            policy,
        })
    }
    Ok(samples)
}
*/

/*
fn run_chess(
    args: &ChessMlArgs,
    boards_sender: std::sync::mpsc::SyncSender<chess::Board>,
    evals_sender: std::sync::mpsc::SyncSender<f64>,
) -> eyre::Result<()> {
    let vs = get_varstore(args)?;
    let model = Arc::new(Mutex::new(model(&vs.root())));
    let mut opt = nn::Sgd::default().build(&vs, 1e-2).unwrap();

    tracing::info!("starting self play");
    if let Some(rounds) = args.self_play_rounds {
        let mut rng = rand::thread_rng();
        let mut games = vec![];
        for _ in 0..rounds {
            let train_sample = generate_self_play_sample(
                args,
                model.clone(),
                &mut rng,
                &boards_sender,
                &evals_sender,
            )?;
            save_sample(args, &train_sample)?;
            {
                let model_guard = model.lock().unwrap();
                let loss = gradient_descent_loss(&model_guard, &train_sample);
                opt.backward_step(&loss);
            };
            games.push(train_sample);

            std::fs::create_dir_all(&args.checkpoint_dir)?;
            vs.save(args.checkpoint_dir.join("self_play")).unwrap();
        }
    }

    tracing::info!("starting training");
    if let Some(rounds) = args.training_rounds {
        let samples = load_samples(args)?;
        let model_guard = model.lock().unwrap();
        for round in 0..rounds {
            for train_sample in tqdm(samples.iter()) {
                let loss = gradient_descent_loss(&model_guard, &train_sample);
                dbg!(&loss);
                opt.backward_step(&loss);
            }

            std::fs::create_dir_all(&args.checkpoint_dir)?;
            tracing::info!("saving");
            vs.save(args.checkpoint_dir.join(format!("{}", round)))
                .expect("error when saving");
        }
    }

    Ok(())
}
*/
