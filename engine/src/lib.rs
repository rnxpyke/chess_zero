pub mod eval;
pub mod model;
pub mod train;
pub mod tree_search;
pub mod boost;
pub mod storage;

use chess::{self, Color};
use tch::Tensor;

#[derive(Debug, PartialEq, Eq)]
pub struct PersonalIndex(pub usize);
pub trait ToPersonalIndex {
    fn to_personal_index(&self, view: chess::Color) -> PersonalIndex;
}

impl ToPersonalIndex for chess::Square {
    fn to_personal_index(&self, view: chess::Color) -> PersonalIndex {
        match view {
            Color::White => PersonalIndex(self.to_index()),
            Color::Black => PersonalIndex(63 - self.to_index()),
        }
    }
}

pub fn chess_board_to_tensor(board: &chess::Board, view: chess::Color) -> Tensor {
    let mut data = vec![0.; 8 * 8 * 6 * 2];

    for square in chess::ALL_SQUARES.into_iter() {
        let PersonalIndex(square_idx) = square.to_personal_index(view);
        if let (Some(piece), Some(color)) = (board.piece_on(square), board.color_on(square)) {
            let piece_idx = piece.to_index();
            assert!(piece_idx < 6);
            let color_idx = if color == view { 0 } else { 1 };
            assert!(color_idx < 2);
            data[color_idx * 6 * 8 * 8 + piece_idx * 8 * 8 + square_idx] = 1f32;
        }
    }
    let tensor = Tensor::from_slice(data.as_slice()).view([2, 6, 8, 8]);
    tensor
}

#[cfg(test)]
mod tests {
    use crate::{chess_board_to_tensor, ToPersonalIndex};

    #[test]
    fn initial_view_is_nearly_identical() {
        use chess::*;
        let _white_view = chess_board_to_tensor(&Board::default(), Color::White);
        let _black_view = chess_board_to_tensor(&Board::default(), Color::Black);
        _white_view.print();
        // black_view.print();
    }

    #[test]
    fn personal_index_identical() {
        use chess::Color;
        assert_eq!(
            chess::Square::A1.to_personal_index(Color::White),
            chess::Square::H8.to_personal_index(Color::Black)
        );
        assert_eq!(
            chess::Square::A2.to_personal_index(Color::White),
            chess::Square::H7.to_personal_index(Color::Black)
        );
        assert_eq!(
            chess::Square::H1.to_personal_index(Color::White),
            chess::Square::A8.to_personal_index(Color::Black)
        );
    }
}
