use std::sync::{
    mpsc::{RecvError, SyncSender},
    Arc, Mutex,
};

use tch::IndexOp;

use crate::{
    chess_board_to_tensor, model::Model, tree_search::Selfish, PersonalIndex, ToPersonalIndex,
};

fn moves_to_policy_mask(moves: &[chess::ChessMove], view: chess::Color) -> tch::Tensor {
    let mut data = vec![-1_000_000_000_000.; 64 * 64];
    for mov in moves.into_iter() {
        let PersonalIndex(source_idx) = mov.get_source().to_personal_index(view);
        let PersonalIndex(dest_idx) = mov.get_dest().to_personal_index(view);
        data[source_idx * 64 + dest_idx] = 0.;
    }

    let tensor = tch::Tensor::from_slice(data.as_slice()).view([64, 64]);
    tensor
}

fn policy_to_moves_eval(
    moves: &[chess::ChessMove],
    policy: &tch::Tensor,
    view: chess::Color,
) -> Vec<f64> {
    let data = {
        let cont = policy.contiguous().view(64 * 64);
        let mut vec = vec![0.0; cont.numel()];
        cont.to_device(tch::Device::Cpu)
            .to_kind(tch::Kind::Double)
            .f_copy_data(&mut vec, cont.numel())
            .unwrap();
        vec
    };
    moves
        .into_iter()
        .map(|mov| {
            match mov.get_promotion() {
                Some(chess::Piece::Queen) => {}
                None => {}
                _ => return 0.,
            };

            let PersonalIndex(source_index) = mov.get_source().to_personal_index(view);
            let PersonalIndex(dest_index) = mov.get_dest().to_personal_index(view);
            data[source_index * 64 + dest_index]
        })
        .collect()
}

pub fn moves_to_policy_label(moves: &[(chess::ChessMove, u64)], view: chess::Color) -> tch::Tensor {
    let mut data = vec![0.; 64 * 64];
    let total_visits: u64 = moves.iter().map(|(_, visits)| *visits).sum();
    for (mov, visits) in moves {
        match mov.get_promotion() {
            Some(chess::Piece::Queen) => {}
            None => {}
            _ => continue,
        };
        let PersonalIndex(source_index) = mov.get_source().to_personal_index(view);
        let PersonalIndex(dest_index) = mov.get_dest().to_personal_index(view);
        data[source_index * 64 + dest_index] = *visits as f32 / total_visits as f32;
    }
    tch::Tensor::from_slice(&data).view([64, 64])
}

fn normalize(moves: &mut [f64]) {
    let sum: f64 = moves.iter().sum();
    if f64::abs(sum - 1.) > 0.1 {
        println!("not normalized!, sum is {}", sum)
    }
    for num in moves {
        *num /= sum;
    }
}

pub struct ModelCommand {
    board: chess::Board,
    view: chess::Color,
    callback: std::sync::mpsc::SyncSender<(tch::Tensor, tch::Tensor)>,
}

pub fn start_model_thread(
    model: Arc<Mutex<Model>>,
) -> (SyncSender<ModelCommand>, std::thread::JoinHandle<()>) {
    let (tx, rx) = std::sync::mpsc::sync_channel::<ModelCommand>(16);

    let handle = std::thread::spawn(move || loop {
        let mut requests = vec![];
        if requests.is_empty() {
            match rx.recv() {
                Ok(request) => requests.push(request),
                Err(RecvError) => break,
            }
        }
        while requests.len() < 16 {
            match rx.try_recv() {
                Ok(request) => requests.push(request),
                Err(_) => break,
            }
        }

        dbg!(requests.len());
        let tensors = requests
            .iter()
            .map(|req| chess_board_to_tensor(&req.board, req.view))
            .collect::<Vec<_>>();
        let inputs = tch::Tensor::stack(&tensors, 0);
        let (values, polices) = tch::no_grad(|| model.try_lock().unwrap().forward_t(&inputs, true));
        for (i, req) in requests.into_iter().enumerate() {
            let value = values.i(i as i64);
            let policy = polices.i(i as i64);
            req.callback.send((value, policy)).unwrap();
        }
    });
    return (tx, handle);
}

pub fn eval_model_thread(
    tx: &SyncSender<ModelCommand>,
    board: chess::Board,
    view: chess::Color,
) -> (tch::Tensor, tch::Tensor) {
    let (tx_res, rx_res) = std::sync::mpsc::sync_channel(0);
    tx.send(ModelCommand {
        board,
        view,
        callback: tx_res,
    })
    .unwrap();
    let res = rx_res.recv().unwrap();
    return res;
}

fn tensor_value_extract(value: tch::Tensor) -> f64 {
    if value.numel() != 1 {
        panic!("value isn't scalar")
    }
    let mut v = [0.0];
    value
        .to_kind(tch::Kind::Double)
        .f_copy_data(&mut v, 1)
        .unwrap();
    v[0]
}

fn tensor_policy_extract(
    policy: tch::Tensor,
    view: chess::Color,
    moves: &[chess::ChessMove],
) -> Vec<f64> {
    let policy_mask = moves_to_policy_mask(&moves, view);

    let policy_masked = policy.view([64, 64]) + policy_mask;
    let policy_eval = policy_masked
        .view([1, 64 * 64])
        .softmax(1, tch::Kind::Float);
    let mut moves_eval = policy_to_moves_eval(moves, &policy_eval, view);
    normalize(&mut moves_eval);
    moves_eval
}

fn join_moves_and_eval(moves: &[chess::ChessMove], moves_eval: &[f64]) -> Vec<MoveValuation> {
    moves_eval
        .into_iter()
        .zip(moves.into_iter())
        .map(|(&value, &mov)| MoveValuation { mov, value })
        .collect()
}

pub fn polish_model_output(
    value: tch::Tensor,
    policy: tch::Tensor,
    moves: &[chess::ChessMove],
    view: chess::Color,
) -> (Vec<f64>, Selfish<f64>) {
    let value_eval: Selfish<f64> = Selfish {
        value: tensor_value_extract(value),
        player: view,
    };

    let moves_eval = tensor_policy_extract(policy, view, moves);
    (moves_eval, value_eval)
}

pub type ModelEndpoint = SyncSender<ModelCommand>;

#[derive(Copy, Clone)]
pub struct EvalInput {
    pub board: chess::Board,
    pub view: chess::Color,
}

pub struct MoveValuation {
    pub mov: chess::ChessMove,
    pub value: f64,
}

pub struct EvalOutput {
    pub evaluation: Selfish<f64>,
    pub policy: Vec<MoveValuation>,
}

pub fn evaluate(model: &Model, EvalInput { board, view }: EvalInput) -> EvalOutput {
    let input = crate::chess_board_to_tensor(&board, view);
    let (value, policy) = tch::no_grad(|| model.forward_t(&input, true));

    let value_eval: Selfish<f64> = Selfish {
        value: tensor_value_extract(value),
        player: view,
    };
    let moves = chess::MoveGen::new_legal(&board).collect::<Vec<_>>();
    let policy_eval = tensor_policy_extract(policy, view, &moves);
    let moves_eval = join_moves_and_eval(&moves, &policy_eval);
    EvalOutput {
        evaluation: value_eval,
        policy: moves_eval,
    }
}

pub fn evaluate_batch<'a>(
    model: &Model,
    inputs: &'a [EvalInput],
) -> impl Iterator<Item = (EvalInput, EvalOutput)> + 'a {
    let tensors = inputs
        .iter()
        .map(|req| chess_board_to_tensor(&req.board, req.view))
        .collect::<Vec<_>>();
    let (values, polices) =
        tch::no_grad(|| model.forward_t(&tch::Tensor::stack(&tensors, 0), true));

    inputs.iter().enumerate().map(move |(i, &req)| {
        let value = values.i(i as i64);
        let policy = polices.i(i as i64);
        let value_eval: Selfish<f64> = Selfish {
            value: tensor_value_extract(value),
            player: req.view,
        };
        let moves = chess::MoveGen::new_legal(&req.board).collect::<Vec<_>>();
        let policy_eval = tensor_policy_extract(policy, req.view, &moves);
        let moves_eval = join_moves_and_eval(&moves, &policy_eval);
        (
            req,
            EvalOutput {
                evaluation: value_eval,
                policy: moves_eval,
            },
        )
    })
}
