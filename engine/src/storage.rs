
pub fn setup_database(conn: &rusqlite::Connection) -> eyre::Result<()> {
    conn.execute(
        "
        CREATE TABLE IF NOT EXISTS boosts
        ( id INTEGER PRIMARY KEY
        , board TEXT NOT NULL
        , nodes INTEGER NOT NULL
        , player BOOL NOT NULL
        , eval REAL NOT NULL
        , policy TEXT NOT NULL
        );
    ",
        (),
    )?;
    conn.execute("
        CREATE TABLE IF NOT EXISTS results
        ( id INTEGER PRIMARY KEY
        , board TEXT NOT NULL
        , score REAL NOT NULL
        );
    ", ())?;
    Ok(())
}

mod policy {
    use std::str::FromStr;

    /// counts how many times each move was visited during rollouts
    pub struct Policy(pub Vec<(chess::ChessMove, u64)>);

    impl Policy {
        pub fn serialize(&self) -> String {
            self.0
                .iter()
                .map(|(mov, visits)| format!("{}:{}", mov, visits))
                .collect::<Vec<_>>()
                .join(",")
        }

        pub fn deserialize(s: &str) -> Self {
            let items = s
                .split(",")
                .map(|s| {
                    let mut splits = s.split(":");
                    let mov = chess::ChessMove::from_str(splits.next().unwrap()).unwrap();
                    let visits: u64 = splits.next().unwrap().parse().unwrap();
                    (mov, visits)
                })
                .collect();
            Self(items)
        }
    }

    #[cfg(test)]
    mod tests {
        use std::str::FromStr;

        #[test]
        fn policy_reserialize() {
            use chess::ChessMove;
            use super::Policy;
            let policy = super::Policy(vec![(ChessMove::from_str("c2c4").unwrap(), 1), (ChessMove::from_str("c2c5").unwrap(), 1)]);
            assert_eq!(policy.0, Policy::deserialize(&policy.serialize()).0);
        }
    }
}

pub use policy::Policy;

pub struct BoostRow {
    pub board: chess::Board,
    pub nodes: u64,
    pub player: chess::Color,
    pub eval: f64,
    pub policy: Policy,
}

pub fn insert_boost(conn: &rusqlite::Connection, sample: BoostRow) -> eyre::Result<()> {
    conn.execute(
        "INSERT INTO boosts (board, nodes, player, eval, policy) VALUES (?1, ?2, ?3, ?4, ?5)",
        (
            &sample.board.to_string(),
            &sample.nodes,
            &(sample.player == chess::Color::White),
            &sample.eval,
            &sample.policy.serialize(),
        ),
    )?;
    Ok(())
}

pub fn insert_result(conn: &rusqlite::Connection, board: chess::Board, score: f64) -> eyre::Result<()> {
    conn.execute(
        "INSERT INTO results (board, score) VALUES (?1, ?2)",
        (&board.to_string(), &score)
    )?;
    Ok(())
}