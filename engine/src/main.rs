use std::fmt::{Debug, Display};
use std::io::{self, BufRead};
use std::sync::{Arc, Mutex};
use std::{path::PathBuf, str::FromStr};

use argh::FromArgs;
use engine::storage::{setup_database, insert_boost, BoostRow, Policy, insert_result};
use eyre;
use rand::{distributions::WeightedIndex, prelude::Distribution};
use tracing::info;
use tracing_subscriber::{fmt, prelude::*, EnvFilter};


use nn::VarStore;
use tch::{nn, Device};

use vampirc_uci::{UciInfoAttribute, UciMessage};

use engine::{model::*, boost};
use engine::tree_search;
use tree_search::ChessMcts;

struct MoveInfoDebug<'a, Spec: mcts::MCTS> {
    info: &'a mcts::MoveInfo<Spec>,
}

impl<Spec: mcts::MCTS> Debug for MoveInfoDebug<'_, Spec>
where
    mcts::Move<Spec>: Display,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.info.visits() == 0 {
            write!(f, "{} [0 visits]", self.info.get_move())
        } else {
            write!(
                f,
                "{} [{} visits] [{} avg reward]",
                self.info.get_move(),
                self.info.visits(),
                self.info.sum_rewards() as f64 / self.info.visits() as f64
            )
        }
    }
}


fn get_varstore(args: &ChessMlArgs) -> eyre::Result<VarStore> {
    let vs = {
        let device = if let Some(id) = args.cuda {
            Device::Cuda(id)
        } else {
            Device::Cpu
        };
        let mut vs = nn::VarStore::new(device);
        if let Some(path) = &args.weights {
            vs.load(path)?;
            println!("loaded network from {:?}", path);
        }
        vs
    };

    Ok(vs)
}

fn sample_move(tree: &mcts::SearchTree<ChessMcts>, rng: &mut impl rand::Rng) -> chess::ChessMove {
    let root = tree.root_node();
    let moves: Vec<_> = root.moves().map(|info| info.get_move()).collect();
    let weights: Vec<_> = root.moves().map(|info| info.visits()).collect();

    let idx = WeightedIndex::new(weights).unwrap().sample(rng);
    *moves[idx]
}

#[derive(FromArgs, Clone)]
/// Train chess ai
struct ChessMlArgs {
    #[argh(positional)]
    weights: Option<PathBuf>,

    /// number of rollouts used in training
    #[argh(option, default = "1600")]
    rollouts: u64,

    /// exploration constant
    #[argh(option, default = "250.")]
    exploration: f64,

    /// directory where to safe the checkpoints
    #[argh(option, default = "PathBuf::from(\"data/network\")")]
    checkpoint_dir: PathBuf,

    /// sample sqlite database
    #[argh(option, default = "PathBuf::from(\"data/samples.db\")")]
    sample_db: PathBuf,

    /// if set, train on GPU using CUDA device
    #[argh(option)]
    cuda: Option<usize>,
}


fn main() -> eyre::Result<()> {
    let env_filter = EnvFilter::from_default_env();
    let fmt_layer = fmt::layer().without_time().with_filter(env_filter);
    tracing_subscriber::registry().with(fmt_layer).init();
    color_eyre::install()?;

    let train_args: ChessMlArgs = argh::from_env();
    info!("Hello, world!");

    let connection = rusqlite::Connection::open(&train_args.sample_db).unwrap();
    setup_database(&connection).unwrap();
    let vs = get_varstore(&train_args)?;
    let model = Arc::new(Mutex::new(model(&vs.root())));

    let mut game = chess::Game::new();
    for line in io::stdin().lock().lines() {
        let msg: UciMessage = vampirc_uci::parse_one(&line.unwrap());
        match msg {
            UciMessage::Uci => {
                println!(
                    "{}",
                    UciMessage::Id {
                        name: Some("chess_zero".into()),
                        author: Some("rnxpyke".into())
                    }
                );
                println!("{}", UciMessage::UciOk)
            }
            UciMessage::Debug(_dbg) => {}
            UciMessage::IsReady => println!("{}", UciMessage::ReadyOk),
            UciMessage::Register { .. } => todo!("handle register"),
            UciMessage::Position {
                fen,
                moves,
                startpos,
            } => {
                if startpos {
                    game = chess::Game::new();
                }
                if let Some(fen) = fen {
                    game = chess::Game::from_str(&fen.0).expect("invalid fen");
                }

                for m in moves {
                    game.make_move(m);
                }
            }
            UciMessage::SetOption { .. } => {}
            UciMessage::UciNewGame => {}
            UciMessage::Stop => {}
            UciMessage::PonderHit => {}
            UciMessage::Quit => {
                break;
            }
            UciMessage::Go {
                time_control: _,
                search_control,
            } => {
                let nodes = search_control
                    .and_then(|a| a.nodes)
                    .unwrap_or(train_args.rollouts);
                let mut mcts = boost::create_mcts(model.clone(), &game, train_args.exploration);
                boost::mcts_boost(
                    &mut mcts,
                    &boost::BoostArgs {
                        rollouts: nodes,
                        info_duration: std::time::Duration::from_millis(250),
                    },
                );

                let (moves, evaluation) = boost::mcts_eval(&mcts);
                insert_boost(
                    &connection,
                    BoostRow {
                        board: game.current_position(),
                        nodes,
                        player: game.side_to_move(),
                        eval: evaluation.0,
                        policy: Policy(moves.iter().map(|b| (b.mov, b.visits)).collect()),
                    },
                )
                .unwrap();
                let best_move_info = moves
                    .into_iter()
                    .max_by(|a, b| a.visits.cmp(&b.visits))
                    .expect("no move found");
                let best_move = best_move_info.mov;
                
                let eval = evaluation.to_selfish(game.side_to_move());
                println!(
                    "{}",
                    UciMessage::Info(vec![UciInfoAttribute::Score {
                        cp: Some(eval.to_centipawns()),
                        mate: None,
                        lower_bound: None,
                        upper_bound: None
                    },])
                );
                println!(
                    "{}",
                    UciMessage::BestMove {
                        best_move,
                        ponder: None
                    }
                );

                check_record_game(&connection, game.clone(), best_move).unwrap();
            }
            UciMessage::Unknown(_a, _) => println!(
                "{}",
                UciMessage::Info(vec![UciInfoAttribute::String("unknown message".into())])
            ),
            other => panic!("unexpected message: {}", &other),
        }
    }

    Ok(())
}

fn check_record_game(connection: &rusqlite::Connection, mut game: chess::Game, best_move: chess::ChessMove) -> eyre::Result<()> {
    game.make_move(best_move);
    let mut score = Option::None;
    if let Some(res) = game.result() {
        score = Some(match res {
            chess::GameResult::WhiteCheckmates => 1.0,
            chess::GameResult::WhiteResigns => -1.0,
            chess::GameResult::BlackCheckmates => -1.0,
            chess::GameResult::BlackResigns => 1.0,
            chess::GameResult::Stalemate => 0.0,
            chess::GameResult::DrawAccepted => 0.0,
            chess::GameResult::DrawDeclared => 0.0,
        });
    } else if game.can_declare_draw() {
        score = Some(0.0);
    }
    if let Some(res) = score {
        info!("storing records");
        let mut pos = chess::Game::new();
        for action in game.actions().iter() {
            match action {
                chess::Action::MakeMove(m) => {
                    pos.make_move(*m);
                    let board= pos.current_position();
                    insert_result(&connection, board, res).unwrap();
                },
                _ => {},
            }
        }
    }
    Ok(())
}
