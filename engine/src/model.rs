use std::borrow::Borrow;

use tch::{
    nn::{self, ConvConfigND, ModuleT},
    Tensor,
};

pub struct Model {
    net: nn::SequentialT,
    value_head: nn::SequentialT,
    policy_head: nn::SequentialT,
}

impl Model {
    pub fn forward_t(&self, input: &Tensor, train: bool) -> (Tensor, Tensor) {
        let xs = input.apply_t(&self.net, train);
        (
            xs.apply_t(&self.value_head, train),
            xs.apply_t(&self.policy_head, train),
        )
    }
}

pub fn model(vs: &nn::Path) -> Model {
    Model {
        net: net(vs / "net"),
        policy_head: policy_head(vs / "policy_head"),
        value_head: value_head(vs / "value_head"),
    }
}

#[derive(Debug)]
pub struct TrainSample {
    pub input: tch::Tensor,
    pub value: tch::Tensor,
    pub policy: tch::Tensor,
}

pub fn stack_samples(samples: Vec<TrainSample>) -> TrainSample {
    let mut inputs = vec![];
    let mut values = vec![];
    let mut policies = vec![];
    for sample in samples.into_iter() {
        inputs.push(sample.input);
        values.push(sample.value);
        policies.push(sample.policy);
    }

    let sample = TrainSample {
        input: tch::Tensor::concat(&inputs, 0),
        value: tch::Tensor::concat(&values, 0),
        policy: tch::Tensor::concat(&policies, 0),
    };

    dbg!(
        sample.input.size(),
        sample.value.size(),
        sample.policy.size()
    );

    sample
}

pub fn gradient_descent_loss(model: &Model, sample: &TrainSample) -> tch::Tensor {
    let (value, policy) = model.forward_t(&sample.input, true);
    let value_loss = value.mse_loss(&sample.value, tch::Reduction::Mean);
    let policy_loss = policy
        .view([-1, 64 * 64])
        .cross_entropy_loss::<tch::Tensor>(
            &sample.policy.view([-1, 64 * 64]),
            None,
            tch::Reduction::Mean,
            -100,
            0.0,
        );
    let loss = value_loss + policy_loss;
    loss.to_kind(tch::Kind::Float)
}

const CONV_DIMS: i64 = 64;

pub fn net<'a, T>(vs: T) -> nn::SequentialT
where
    T: Borrow<nn::Path<'a>>,
{
    let vs: &nn::Path<'a> = vs.borrow();
    nn::seq_t()
        .add_fn(|xs| xs.view([-1, 6 * 2, 8, 8]))
        .add(conv_layer(vs / "init_conv", 6 * 2, CONV_DIMS, 3))
        .add_fn(|xs| xs.relu())
        .add(res_layer(vs / "res1"))
        .add(res_layer(vs / "res2"))
        .add(res_layer(vs / "res3"))
        .add(res_layer(vs / "res4"))
        .add(res_layer(vs / "res5"))
        .add(res_layer(vs / "res6"))
        .add(res_layer(vs / "res7"))
        .add(res_layer(vs / "res8"))
        .add(res_layer(vs / "res9"))
}

pub fn value_head<'a, T>(vs: T) -> nn::SequentialT
where
    T: Borrow<nn::Path<'a>>,
{
    let vs: &nn::Path<'a> = vs.borrow();
    nn::seq_t()
        .add(nn::conv2d(
            vs / "conv1_1",
            CONV_DIMS,
            CONV_DIMS,
            1,
            Default::default(),
        ))
        .add(nn::batch_norm2d(vs / "norm", CONV_DIMS, Default::default()))
        .add_fn(|xs| xs.relu())
        .add_fn(|xs| xs.view([-1, CONV_DIMS, 8 * 8]))
        .add(nn::linear(vs / "full_conn1", 64, 1, Default::default()))
        .add_fn(|xs| xs.view([-1, CONV_DIMS]))
        .add_fn(|xs| xs.relu())
        .add(nn::linear(
            vs / "to_scalar",
            CONV_DIMS,
            1,
            Default::default(),
        ))
        .add_fn(|xs| xs.tanh())
        .add_fn(|xs| xs.view([-1]))
}

pub fn policy_head<'a, T>(vs: T) -> nn::SequentialT
where
    T: Borrow<nn::Path<'a>>,
{
    let vs: &nn::Path<'a> = vs.borrow();
    nn::seq_t()
        .add(nn::conv2d(
            vs / "conv1_1",
            CONV_DIMS,
            2,
            1,
            Default::default(),
        ))
        .add(nn::batch_norm2d(vs / "norm", 2, Default::default()))
        .add_fn(|xs| xs.relu())
        .add_fn(|xs| xs.view([-1, 2 * 8 * 8]))
        .add(nn::linear(
            vs / "output",
            2 * 8 * 8,
            64 * 64,
            Default::default(),
        ))
        .add_fn(|xs| xs.view([-1, 8, 8, 8, 8]))
}

pub fn conv_layer<'a, T>(vs: T, i: i64, o: i64, k: i64) -> nn::SequentialT
where
    T: Borrow<nn::Path<'a>>,
{
    let vs: &nn::Path<'a> = vs.borrow();
    let norm_out_dim = CONV_DIMS;
    nn::seq_t()
        .add(nn::conv2d(
            vs / "conv",
            i,
            o,
            k,
            ConvConfigND {
                padding: 1,
                ..Default::default()
            },
        ))
        .add(nn::batch_norm2d(
            vs / "norm",
            norm_out_dim,
            Default::default(),
        ))
}

pub fn skip_connection<'a>(pre: impl ModuleT + 'a) -> nn::FuncT<'a> {
    nn::func_t(move |xs, train| {
        let ys = xs.apply_t(&pre, train);
        xs + ys
    })
}

pub fn res_layer<'a, T>(vs: T) -> nn::SequentialT
where
    T: Borrow<nn::Path<'a>>,
{
    let vs: &nn::Path<'a> = vs.borrow();
    nn::seq_t()
        .add(skip_connection(
            nn::seq_t()
                .add(conv_layer(vs / "conv1", CONV_DIMS, CONV_DIMS, 3))
                .add_fn(|xs| xs.relu())
                .add(conv_layer(vs / "conv2", CONV_DIMS, CONV_DIMS, 3)),
        ))
        .add_fn(|xs| xs.relu())
}
