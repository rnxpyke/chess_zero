use std::{sync::{Arc, Mutex}, time::Instant};

use mcts::{MCTSManager, GameState, tree_policy::AlphaGoPolicy, transposition_table::ApproxTable};
use vampirc_uci::{UciMessage, UciInfoAttribute};

use crate::tree_search;
use tree_search::{ChessMcts, Selfish, move_reward_norm, ChessEvaluator, Objective};
use crate::model::Model;



fn go_info(mcts: &MCTSManager<ChessMcts>) {
    let pv = mcts.principal_variation(5);
    let game = mcts.tree().root_state();
    let state = mcts.principal_variation_info(1)[0];
    let rewards = Selfish {
        player: game.current_player(),
        value: move_reward_norm(state),
    };
    {
        let nodes = mcts.tree().num_nodes();
        println!(
            "{}",
            UciMessage::Info(vec![
                UciInfoAttribute::Score {
                    cp: Some(rewards.to_centipawns()),
                    lower_bound: None,
                    upper_bound: None,
                    mate: None
                },
                UciInfoAttribute::Nodes(nodes as u64),
                UciInfoAttribute::Pv(pv),
            ])
        );
    }
}

pub struct BoostArgs {
    pub rollouts: u64,
    pub info_duration: std::time::Duration,
}

#[derive(Debug)]
#[allow(unused)]
pub struct BoostMoveEval {
    pub mov: chess::ChessMove,
    pub visits: u64,
    pub sum_rewards: i64,
    pub mov_eval: f64,
}

pub fn create_mcts(
    model: Arc<Mutex<Model>>,
    game: &chess::Game,
    exploration: f64,
) -> MCTSManager<ChessMcts> {
    let evaluator = ChessEvaluator::from_arc(model);
    MCTSManager::new(
        tree_search::ChessGame(game.clone()),
        ChessMcts {},
        evaluator,
        AlphaGoPolicy::new(exploration),
        ApproxTable::new(1024),
    )
}

pub fn mcts_boost(mcts: &mut MCTSManager<ChessMcts>, args: &BoostArgs) {
    let mut start = Instant::now();
    for _ in 0..args.rollouts {
        mcts.playout();
        let elapsed = Instant::now() - start;
        if elapsed > args.info_duration {
            start = Instant::now();
            go_info(&mcts);
        }
    }
    go_info(&mcts);
}

pub fn mcts_eval(mcts: &MCTSManager<ChessMcts>) -> (Vec<BoostMoveEval>, Objective<f64>) {
    let player = mcts.tree().root_state().current_player();
    let evaluation = {
        let var = mcts.principal_variation_info(1)[0];
        Selfish {
            player,
            value: move_reward_norm(var),
        }
        .to_objective()
    };
    let root = mcts.tree().root_node();
    let mut moves = root
        .moves()
        .map(|m| BoostMoveEval {
            mov: *m.get_move(),
            visits: m.visits(),
            sum_rewards: m.sum_rewards(),
            mov_eval: *m.move_evaluation(),
        })
        .collect::<Vec<_>>();
    moves.sort_by(|a, b| a.visits.cmp(&b.visits));
    return (moves, evaluation);
}